//
//  main.swift
//
//
//  Created by Logan Radloff on 1/8/22.
//

import WhollyFullySwiftCore

let wfs = WhollyFullySwift()

do {
    try wfs.run()
} catch {
    print("Whoops! An error occurred: \(error)")
}
