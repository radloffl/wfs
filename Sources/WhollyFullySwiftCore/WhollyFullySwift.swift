//
//  WhollyFullySwift.swift
//  
//
//  Created by Logan Radloff on 1/8/22.
//

import Foundation

public final class WhollyFullySwift {
    private let arguments: [String]

    public init(arguments: [String] = CommandLine.arguments) {
        self.arguments = arguments
    }

    public func run() throws {
        print("Hello world")
    }
}
